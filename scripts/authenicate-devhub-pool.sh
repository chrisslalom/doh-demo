#!/bin/bash
# Usage: sh scripts/url-login.sh

# Import functions
. ./scripts/lib/sfdx.sh

url_login $DEVHUB1URL $DEVHUB1ALIAS
url_login $DEVHUB2URL $DEVHUB2ALIAS
get_info