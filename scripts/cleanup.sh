#!/bin/bash
# Usage: sh scripts/cleanup.sh

# Import defaults
. ./scripts/lib/defaults.sh

# Import functions
. ./scripts/lib/sfdx.sh

# Main script starts here
SCRATCH_ORG_NAME="${1:-validation}"
echo "*** Deleting $SCRATCH_ORG_NAME"
delete_org $SCRATCH_ORG_NAME
