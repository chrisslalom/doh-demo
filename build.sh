# Exit if any command fails
set -e

# Get default config values from scratch config file
#source .scratchconfig

DEV_HUBS="devhub1,devhub2"
SCRATCH_DEFAULT_ALIAS="doh-scratch"
SCRATCH_DEFAULT_DURATION=7

# Create a scratch org from a pool of authorized dev hubs.
echo "Creating a new scratch org..."
sfdx devhubpool:org:create -f config/project-scratch-def.json -d "${SCRATCH_DEFAULT_DURATION:-1}" --setdefaultusername -p "${DEV_HUBS:-hub}" -a "${SCRATCH_DEFAULT_ALIAS:-my_scratch_org}"

# Deploy the project to the scratch org.
echo "Deploying to scratch org..."
sfdx force:source:deploy -p force-app

# Assign the perm set to the scratch org user
#echo "Assigning permissions..."
#sfdx force:user:permset:assign -n ScratchOrgPermSet

# Display the scratch org info and access url
echo "Scratch org details:"
sfdx force:org:display --verbose
sfdx force:org:open -r

# Open the scratch org in the browser
echo "Opening the scratch org in the browser..."
sfdx force:org:open
